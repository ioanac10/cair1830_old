package salariati.validator;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		boolean isFirstNameValid  = employee.getFistName().matches("[a-zA-Z]+") && (employee.getFistName().length() > 2) && (employee.getFistName().length() < 25);
		boolean isLastNameValid  = employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 2) && (employee.getLastName().length() < 25);
		boolean isCNPValid       = employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER)  ||
								   employee.getFunction().equals(DidacticFunction.CONFERENTIAR);
		boolean isSalaryValid    = (tryParseInt(employee.getSalary()));
		if(isSalaryValid == true)
		{
			isSalaryValid = Integer.parseInt(employee.getSalary()) > 0;
		}
		
		return isFirstNameValid && isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
	}

	boolean tryParseInt(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	
}
