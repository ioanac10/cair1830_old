package salariati.test.validator;

import org.junit.Before;
import salariati.exception.EmployeeException;
import salariati.model.*;

import org.junit.Test;
import salariati.repository.implementations.EmployeeRepositoryImpl;

import static org.junit.Assert.*;

public class testBB {
    EmployeeRepositoryImpl repo;

    @Before
    public void setUp() throws Exception {
         repo = new EmployeeRepositoryImpl();

    }

    @Test
    public void test1() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("caraian;ioana;2960212125795;ASISTENT;2000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test2() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;maria;2961603111222;LECTURER;3000",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test3() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("a222;ioana;2961603111223;ASISTENT;2000",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test4() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("marinescu;alex;18901011234;LECTURER;3000",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test5() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("p;marian;1800101123456;ASISTENT;2000",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test6() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("ionescu;ai;2860303123999;ASISTENT;2000",0);
        assertEquals(e.getFistName(),"");
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test7() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("vasilescu;andrei;1970909123908;STUDENT;1500",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test8() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("andreica;maria;2780212789123;LECTURER;300a",0);
        repo.addEmployee(e);

    }

    @Test(expected = EmployeeException.class)
    public void test9() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("caraian;ana;2780212789aaa;ASISTENT;2000",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test10() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;a111a;2950505111222;ASISTENT;2000",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test11() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;ana;2981126368019;LECTURER;0",0);
        repo.addEmployee(e);
    }

    @Test(expected = EmployeeException.class)
    public void test12() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;marius;1920404335321;LECTURER;abc",0);
    }

    @Test(expected = Exception.class)
    public void test13() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("po;andrei;1720101414766;LECTURER;3000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test14() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("poop;denisa;2890101415827;TEACHER;4000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test15() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pooooooooooooooooooooop;ana;2890101416562;CONFERENTIAR;4000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test16() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("poooooooooooooooooooooop;ana;2890101416562;CONFERENTIAR;4000",0);
        repo.addEmployee(e);
    }

    @Test(expected = Exception.class)
    public void test17() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pooooooooooooooooooooooop;ana;2890101416562;CONFERENTIAR;4000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test18() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("nume;ioan;1900527061443;LECTURER;3000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test19() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("nume;ioooooooooooooooooooooa;2931121044449;TEACHER;3000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test20() throws EmployeeException {
        Employee e = new Employee();
        e = e.getEmployeeFromString("nume;iooooooooooooooooooooooa;2911107341788;LECTURER;3000",0);
        repo.addEmployee(e);
    }

    @Test(expected = Exception.class)
    public void test21() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("nume;ioooooooooooooooooooooooa;2950921528404;LECTURER;4000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test22() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;ana;2940516376309;LECTURER;2000",0);
        repo.addEmployee(e);
    }

    @Test
    public void test23() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;ana;1900120148661;LECTURER;2147483646",0);
        repo.addEmployee(e);
    }

    @Test
    public void test24() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;ana;1900120148661;LECTURER;2147483647",0);
        repo.addEmployee(e);
    }


    @Test(expected = Exception.class)
    public void test25() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("pop;ana;1960921392382;LECTURER;1930501353668",0);
        repo.addEmployee(e);
    }
}
