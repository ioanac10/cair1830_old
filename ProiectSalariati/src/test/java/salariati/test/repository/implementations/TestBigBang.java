package salariati.test.repository.implementations;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;

import java.util.List;

public class TestBigBang {
    EmployeeRepositoryImpl repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeRepositoryImpl();

    }

    @Test
    public void testA() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("caraian;ioana;2960212125795;ASISTENT;2000",0);
        repo.addEmployee(e);
        Employee empl = repo.findEmployeeByCnp("2960212125795");
        assertEquals(empl.getFistName(),"caraian");
        assertEquals(empl.getLastName(),"ioana");
        assertEquals(empl.getFunction(), DidacticFunction.ASISTENT);
    }

    @Test
    public void testB() throws Exception {
        Employee oldEmployee = new Employee();
        oldEmployee = oldEmployee.getEmployeeFromString("caraian;ioana;2960212125795;ASISTENT;2000",0);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("caraian;ioana;2960212125795;TEACHER;2000",0);
        repo.modifyEmployee(oldEmployee,newEmployee);
        Employee e = repo.findEmployeeByCnp("2960212125795");
        assertEquals(e.getFunction(), DidacticFunction.TEACHER);
    }

    @Test
    public void testC() throws Exception {
        List<Employee> listEmployee = repo.getEmployeeByCriteria();
        assertEquals(listEmployee.size() != 0, true);
    }

    @Test
    public void testP() throws Exception {
        Employee e = new Employee();
        e = e.getEmployeeFromString("nume;ioan;1900527061443;LECTURER;3000",0);
        repo.addEmployee(e);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("nume;ioan;1900527061443;CONFERENTIAR;3000",0);
        repo.modifyEmployee(e,newEmployee);
        List<Employee> listEmployee = repo.getEmployeeByCriteria();
        assertEquals(listEmployee.size() != 0, true);
        for(Employee empl : listEmployee)
            if(empl.getCnp().equals("1900527061443"))
                assertEquals(empl.getFunction(), DidacticFunction.CONFERENTIAR);
    }
}
