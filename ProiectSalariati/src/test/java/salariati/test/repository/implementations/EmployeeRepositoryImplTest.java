package salariati.test.repository.implementations;

import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;

import static org.junit.Assert.*;

public class EmployeeRepositoryImplTest {
    EmployeeRepositoryImpl repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeRepositoryImpl();

    }

    @Test
    public void test1() throws Exception {
        Employee oldEmployee = new Employee();
        oldEmployee = oldEmployee.getEmployeeFromString("poop;denisa;2890101415899;TEACHER;4000",0);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("poop;denisa;2890101415899;LECTURER;5000",0);
        repo.modifyEmployee(oldEmployee,newEmployee);
        Employee e = repo.findEmployeeByCnp("2890101415899");
        assertEquals(e,null);
    }


    @Test
    public void test2() throws Exception {
        populateEmployees();

        Employee oldEmployee = new Employee();
        oldEmployee = oldEmployee.getEmployeeFromString("poop;denisa;2890101415827;TEACHER;4000",0);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("poop;denisa;2890101415827;LECTURER;5000",0);
        repo.modifyEmployee(oldEmployee,newEmployee);
        Employee e = repo.findEmployeeByCnp("2890101415827");
        assertEquals(e.getFunction(), DidacticFunction.LECTURER);
        assertEquals(e.getSalary(),"5000");
    }

    @Test
    public void test3() throws Exception {
        Employee oldEmployee = new Employee();
        oldEmployee = oldEmployee.getEmployeeFromString("poop;denisa;2890101415899;TEACHER;4000",0);
        Employee newEmployee = new Employee();
        newEmployee = newEmployee.getEmployeeFromString("poop;denisa;2890101415899;LECTURER;5000",0);
        repo.modifyEmployee(oldEmployee,newEmployee);
        Employee e = repo.findEmployeeByCnp("2890101415899");
        assertEquals(e,null);
    }

    private void populateEmployees() {
        try {
            Employee e = new Employee();
            e = e.getEmployeeFromString("caraian;ioana;2960212125795;ASISTENT;2000", 0);
            repo.addEmployee(e);
            e = e.getEmployeeFromString("poop;denisa;2890101415827;TEACHER;4000", 0);
            repo.addEmployee(e);
            e = e.getEmployeeFromString("pop;ana;2940516376309;LECTURER;2000", 0);
            repo.addEmployee(e);
        }catch (Exception ex)
        {

        }
    }
}